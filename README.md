# Frontend Mentor - Notifications page solution

This is a solution to the [Notifications page challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/notifications-page-DqK5QAmKbC). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

### Links

- Live Site URL: [netilfy](https://imaginative-peony-fca243.netlify.app)


## Author
Frontend Mentor - [@ernest](https://www.frontendmentor.io/profile/Ernestsomto)